# RestFullManifest
Hello Friends in This section discusses about standards rules for the [restFull API](https://en.wikipedia.org/wiki/Representational_state_transfer)

# tabel of contents

* [restFull API](https://en.wikipedia.org/wiki/Representational_state_transfer) authentication
* naming in [restFull API](https://en.wikipedia.org/wiki/Representational_state_transfer)
* using [Request Method](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods)  as right way
* Use [HTTP](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol) headers for serialization formats
* Use HATEOAS (<b>H</b>ypermedia <b>a</b>s <b>t</b>he <b>E</b>ngine <b>o</b>f <b>A</b>pplication <b>S</b>tate)
* Provide filtering, sorting, field selection and paging for collections
* Field selection
* Use RESTful URLs and actions
* Paging
* Version your API
* [HTTP](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol) Status codes.
* [JSON](https://en.wikipedia.org/wiki/JSON)
* Field name casing convention
* [SSL](https://en.wikipedia.org/wiki/Transport_Layer_Security) everywhere - all the time
* Documentation